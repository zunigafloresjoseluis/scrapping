#-*- coding:utf-8 -*-

alfabeto_mayuscula={
'A':'T',
'B':'U',
'C':'Y',
'D':'W',
'E':'X',
'F':'Y',
'G':'Z',
'H':'A',
'I':'B',
'J':'C',
'K':'D',
'L':'E',
'M':'F',
'N':'G',
'O':'H',
'P':'I',
'Q':'J',
'R':'K',
'S':'L',
'T':'M',
'U':'N',
'V':'O',
'W':'P',
'X':'Q',
'Y':'R',
'Z':'S'


}
def cifrar():
    palabra=raw_input('\n ingrese la palabra en MAYUSCULA\n')
    nueva_palabra=' '
    for i in range(len(palabra)):
        nueva_palabra+=alfabeto_mayuscula[palabra[i]]
    return nueva_palabra

def descifrar():
    palabra=raw_input('Ingrese mensaje cifrado en MAYSUCULA')
    nueva_palabra=' ';
    for i in range(len(palabra)):
        for value,key in alfabeto_mayuscula.iteritems():
            if (palabra[i]==value):
                nueva_palabra+=key
    return nueva_palabra
            

def run():
   while True:
       opcion=raw_input('''   
        -------------*-------------------*---
        #seleccione una opcion
        [c]ifrar
        [d]escifrar
        [s]alir
       ''')

       if opcion=='c':
           print(cifrar())
       if opcion=='d':
           print(descifrar())
       if opcion=='s':
            break
       

if __name__=='__main__':
    run()