#-*- coding:utf-8 -*-

def run():
    menu()

def menu():
    _palindromo=str(raw_input('Ingrese la cadena \n '))
    if(_es_palindromo(_palindromo)==True):
        print('{} es palindromo'.format(_palindromo))
    else:
        print('{} no es palindromo'.format(_palindromo))

def _es_palindromo(_string):
    _string=_space(_string)
    aux=_space(_string[::-1])
    for letter in range(len(_string)):
        if aux[letter]!=_string[letter]:
            return False
    
    return True

def _space(_string):
    new_str=''
    for letter in range(len(_string)):
        if(_string[letter]!=' '):
            new_str+=_string[letter]
    
    return new_str

if __name__=='__main__':
    run()
