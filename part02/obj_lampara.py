#-*- coding:utf-8 -*-

class Carro:
    #   en clases  siempre lleva
    #   este metodo  es el metodo de  instancia
    # es el constructor

    #******* Variables ********
    
    def __init__(self,num_llantas,color,prendido,gas,agua):
        self._numero_llantas=num_llantas
        self._color=color
        self._prendido=prendido
        self._litros_gas=gas
        self._cant_agua=agua
    
    def getNumero_llantas(self):
        return self._numero_llantas

    def getColor(self):
        return self._color
    def setNumero_llantas(self,num_llantas):
        self._numero_llantas=num_llantas
        
def run():
    carro=Carro(num_llantas=10,color="rojo",prendido=True,gas=100,agua=0)   
    print("numero de llantas {}".format(carro.getNumero_llantas()))    
    carro.setNumero_llantas(40)
    print("numero de llantas {}".format(carro.getNumero_llantas()))    
  

if __name__=='__main__':
    run()