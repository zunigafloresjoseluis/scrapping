# -*- coding:utf-8 -*-

class Carro:
    #   en clases  siempre lleva
    #   este metodo  es el metodo de  instancia
    # es el constructor

    #******* Variables ********
    
    def __init__(self,num_llantas,color,prendido,gas,agua):
        self._numero_llantas=num_llantas
        self._color=color
        self._prendido=prendido
        self._litros_gas=gas
        self._cant_agua=agua
    
    def getNumero_llantas(self):
        return self._numero_llantas

    def getColor(self):
        return self._color
    def setNumero_llantas(self,num_llantas):
        self._numero_llantas=num_llantas
 
